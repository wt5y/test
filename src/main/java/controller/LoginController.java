package controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.UserInfo;
import service.UserInfoService;

@RestController
public class LoginController {
	
	@Autowired
	private UserInfoService userInfoService;
	
	@SuppressWarnings("unused")
	@RequestMapping("/submit")
	public String submit(HttpServletRequest request){
		Map<String, String[]> paramMap=new HashMap<String, String[]>();
		String result=new String();
		HashMap<String,Object> queryMap=new HashMap<>();
		paramMap=request.getParameterMap();
		/**
		 * 将paramMap变成一个查询数据库的参数，并插入queryMap中，具体是
		 * name String 姓名
		 * sex String 性别
		 * birthday Date 出生日期
		 * 如果姓名为空或者性别为空或者出生日期晚于今天，则返回具体错误给字符串result
		 * 如果没有问题就返回“成功”
		 */
		try{
			userInfoService.insertByMap(queryMap);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return null;
	}
	
	@RequestMapping("/list")
	public List<UserInfo> selectForList(){
		return userInfoService.selectForList();
	}
	
	@RequestMapping("/delete")
	public String deleteOne(HttpServletRequest request){
		Map<String, String[]> paramMap=request.getParameterMap();
		long queryId=0L;
		/**
		 * 将paramMap的id的值赋给queryId，进行删除
		 */
		userInfoService.deleteOne(queryId);
		return null;
	}

}
