package controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserInfoService;

@Controller
@ResponseBody
public class TestController {
	
	@Autowired
	private UserInfoService userInfoService;
	
	@RequestMapping("/index")
	public String helloWorld(){
		System.out.println("hello world");
		return "hello world";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("/test")
	public String test() {
		HashMap map=new HashMap<String,Object>();
		map.put("name", "zhoubotao");
		map.put("sex", "male");
		userInfoService.insertByMap(map);
		return "success";
	}

}
