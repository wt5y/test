package configuration;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class mybatisConfiguration {
	
	@Bean
	public SqlSessionFactory sqlSessionFactory(){
		String resource="mybatis-config.xml";
		try(InputStream inputStream=Resources.getResourceAsStream(resource)) {
			return new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Bean
	@Scope("prototype")
	public SqlSession sqlSession() {
		return sqlSessionFactory().openSession(true);
	}

}
