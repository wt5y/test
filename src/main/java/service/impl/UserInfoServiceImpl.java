package service.impl;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.UserInfo;
import repository.UserInfoMapper;
import service.UserInfoService;

@Service
public class UserInfoServiceImpl implements UserInfoService {
	
	@Autowired
	private SqlSession session;
			
	@SuppressWarnings("rawtypes")
	@Override
	public int insertByMap(HashMap map) {
		UserInfoMapper userInfoMapper=session.getMapper(UserInfoMapper.class);
		return userInfoMapper.insertByMap(map);
	}
	
	@Override
	public List<HashMap<String,String>> selectForCity(){
		UserInfoMapper userInfoMapper=session.getMapper(UserInfoMapper.class);
		return userInfoMapper.selectForCity();
	}
	
	@Override
	public List<UserInfo> selectForList(){
		UserInfoMapper userInfoMapper=session.getMapper(UserInfoMapper.class);
		return userInfoMapper.selectForList();
	}
	
	@Override
	public void deleteOne(long id){
		UserInfoMapper userInfoMapper=session.getMapper(UserInfoMapper.class);
		userInfoMapper.deleteOne(id);
	}

}
