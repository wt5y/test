package service;

import java.util.HashMap;
import java.util.List;

import model.UserInfo;

public interface UserInfoService {
		
	@SuppressWarnings("rawtypes")
	public int insertByMap(HashMap map);
	
	public List<HashMap<String,String>> selectForCity();
	
	public List<UserInfo> selectForList();
	
	public void deleteOne(long id);

}
