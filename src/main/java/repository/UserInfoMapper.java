package repository;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import model.UserInfo;

@Repository
public interface UserInfoMapper {
	
	@SuppressWarnings("rawtypes")
	public int insertByMap(HashMap map);
	
	public List<HashMap<String,String>> selectForCity();
	
	public List<UserInfo> selectForList();
	
	public void deleteOne(long id);

}
